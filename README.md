# Prometheus-Monitoring-Nodejs-Application

#### Project Outline

In the previous projects we have configured monitoring at the infastrcuture level for the nodes, monitoring for the K8s components at the platform level and monitoring for the 3rd party applications, now in this project we will monitor our own application at the application level which will be a very simple nodejs application.

So we will need to define custom metrics where we will use Prometheus client libraries, which will match the language the application is written in and is an abstract interface to expose our metrics. There are different metric types that can be implemented such as Counter, Gauge, Histogram and so on.

So the steps to monitor own application, will include exposing metrics for our Nodejs application using Nodejs client library, then deploy Nodejs app in the cluster, can then configure Prometheus to scrape new target using service monitor. Can then visualize the scraped metrics in a Grafana dashboard.

Below is the architecture we will implement.

![Image 1](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image1.png)

#### Lets get started

Lets navigate to our nodejs application and install npm and start the app

```
npm install express,
node app/server.js
```

![Image 2](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image2.png)

And access the application on localhost:3000

![Image 3](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image3.png)

Now we want to expose metrics of the application we will expose two
1.	Number of requests
2.	Duration of requests

So as DevOps Engineer, can request the development team to expose metrics in the application, and therefore include the Prometheus Client Library in the code

Lets say the Development team have already added the Prometheus client library to the code

And here we have it in the package.json

![Image 4](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image4.png)

Below is a link on how developers can incorporate the Prometheus-client in their code

https://www.npmjs.com/package/prom-client

Once added to the dependencies it can now be used in the code

Can see the Prometheus client being imported and exposing basic metrics

![Image 5](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image5.png)

Now we can expose our intended defined metrics

![Image 6](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image6.png)

Now that they are exposed, we need to track the metrics, and the logic below does that

![Image 7](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image7.png)

So in a Developers world, we need to define the metric and then track the value on our logic.

We can also see the endpoint being used

```
http://localhost:3000/metrics
```

And can see the below how they are being recorded

![Image 8](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image8.png)

Can also see the defined metrics which be utilized in Prometheus

![Image 9](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image9.png)

Let’s make another request can see the number of requests increase

![Image 10](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image10.png)

Now lets connect it to Prometheus which we can track and monitor for this data
And start scraping the above metrics

Next step we will Build Docker Image & Push to repo
1)	Build Docker Image
Nodejsapp -> build > image

2)	Push to private Docker repository
Image -> Push -> dockerhub
And then deploy it in Kubernetes cluster

Can then configure the below dockerfile

![Image 11](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image11.png)

Can then run the below code

```
docker build -t fuad95/nodejs-app:prometheus .
```

Can see it has been successfully built

![Image 12](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image12.png)

And then login to docker

```
docker login
```

![Image 13](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image13.png)

Can then push the image

```
docker push fuad95/nodejs-app:Prometheus
```

Can now see the image in my repository

![Image 14](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image14.png)

Now lets deploy the image in our Kubernetes cluster

We will use a basic Deployment/Service file

![Image 15](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image15.png)

Further configuration

![Image 16](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image16.png)

Now we want Kubernetes to pull the image and access the repository

We can use and create secret

Using the below command

```
kubectl create secret docker-registry my-registry-key --docker-username=fuad95 --docker-password=XXXX
```

And can see it has now been created

![Image 17](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image17.png)

Can then add to the k8s file so that it can pull the image

![Image 18](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image18.png)

Can then apply the configuration

![Image 19](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image19.png)

Can see the pod

![Image 20](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image20.png)

Can then get the service

![Image 21](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image21.png)

And can do port forwarding from pod port to local port

![Image 22](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image22.png)

And then access the application

![Image 23](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image23.png)

Lets refresh a few times and can see the metrics being exposed and tracked

![Image 24](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image24.png)

Next we will tell Prometheus to start scraping and display in Grafana dashboard

Similar to the Redis monitoring project, we need to implement ServiceMonitor component which will provide a link between Prometheus and a new endpoint.

Next lets configure the Service monitor to our k8s config file

![Image 25](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image25.png)

Next, we need to define when Prometheus discovers this service monitor and allow Prometheus to discover the endpoint

Can include the path for the metrics for Prometheus to scrape

Aswell as naming the service so that it can be referenced in the endpoints

![Image 26](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image26.png)

Also including the port aswell

![Image 27](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image27.png)

Next we need to define the namespace selector where the nodejs-app pods are in the default namespace and the Prometheus stack is in it’s own namespace

So since the Service monitor is in a different namespace, we need to configure the namespace using namespaceSelector attribute and then connect to the app using match labels

![Image 28](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image28.png)

Now ready to apply the file

![Image 29](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image29.png)

Can see it got updated and Prometheus was able to connect to the service monitor

![Image 30](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image30.png)

Can see the metrics aswell

![Image 31](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image31.png)

We can also visualize the queries in the Prometheus UI with the total number of requests

![Image 32](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image32.png)

But for better representation lets use Grafana and use the below promQL

```
rate(http_request_operations_total[2m])
```

![Image 33](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image33.png)

And can see the dashboard

![Image 34](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image34.png)

Can then do some more testing and see a spike in requests

![Image 35](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image35.png)

Can also run the below to see the efficiency of the request to see if it took too long or was quick

```
rate(http_request_duration_seconds_sum[2m])
```

Can also import that into Grafana

And there we have it

The Request Per Second and Request Duration dashboards completed

![Image 36](https://gitlab.com/FM1995/prometheus-monitoring-nodejs-application/-/raw/main/Images/Image36.png)













